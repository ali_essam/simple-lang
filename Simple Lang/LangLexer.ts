/// <reference path="Language.ts"/>

module Language {

    export enum TokenTypes {
        INVALID, NUMBER, ID, STRING, EOL, INDENT, DEDENT, L_PARA, R_PARA, COMMA, EQUAL, NOT_EQUAL,
        GREATER, GREATER_EQUAL, SMALLER, SMALLER_EQUAL, PLUS, MINUS, MUL, DIV, MOD, POW,
        IF, WHILE, ELSE, END, THEN, DRAW, CIRCLE, RECTANGLE,LINE,TEXT, WITH, AT, FROM, TO, LET, BE, AND, OR, NOT, EOF,
        FOR,IN,DO
    }

    enum charTypes {
        SKIP, NUM, DOT, OTHER, LETTER, DELI, EOL, OP
    }
    export class Lexer {
        private p: number = 0;
        private depth: number = 0;
        private lineNum: number = 0;
        private colNum: number = 0;
        private code: string;
        private Tokens: Token[] = new Array();
        private reservedMap: TokenTypes[]= new Array();

        constructor() {
            this.initReserved();
        }

        public updateCode(_code: string) {
            this.code = _code + '\n';
        }

        //Tools

        private hasMoreChars() {
            if (this.p >= this.code.length) {
                return false;
            }
            else {
                return true;
            }
        }

        private reset() {
            this.depth = 0;
            this.p = 0;
            this.lineNum = 0;
            this.colNum = 0;
        }

        private move() {
            this.p++;
            this.colNum++;
        }

        private getCharType(c: string) {
            if (c == ' ' || c == '\t' || c == '\r\n' || c == '\r') {
                return charTypes.SKIP;
            }
            else if (c == '\n') {
                return charTypes.EOL;
            }
            else if (c >= '0' && c <= '9') {
                return charTypes.NUM;
            }
            else if ((c >= 'a' && c <= 'z') || (c >= 'A' && c <= 'Z')) {
                return charTypes.LETTER;
            }
            else if (c == '.') {
                return charTypes.DOT;
            }
            else if (c == '(' || c == ')' || c == '{' || c == '}' || c == '[' || c == ']' || c == ',') {
                return charTypes.DELI;
            }
            else if (c == '+' || c == '-' || c == '*' || c == '/' || c == '%' || c == '!' || c == '=' || c == '&' || c == '|' || c == '>' || c == '<' || c=='^') {
                return charTypes.OP;
            }
            else {
                return charTypes.OTHER;
            }
        }

        private match(type: charTypes) {
            if (type == this.getCharType(this.peek())) {
                return true;
            }
            else {
                return false;
            }

        }

        private peek() {
            return this.code.charAt(this.p);
        }

        private read() {
            this.move();
            return this.code.charAt(this.p - 1);
        }

        private acceptToken(lex: string, type: TokenTypes, error: string= "") {
            this.Tokens.push(new Token(lex, type, this.p - lex.length, this.lineNum, this.colNum - lex.length, error));
        
        }

        //END Tools


        //Lex Tools
        private lexNum() {
            var tmp: string = "";
            var dotFound: boolean = false;
            while (this.hasMoreChars()) {
                if (this.getCharType(this.peek()) == charTypes.NUM) {
                    tmp += this.read();
                }
                else if (!dotFound && this.peek() == '.') {
                    dotFound = true;
                    tmp += this.read();
                }
                else {
                    this.acceptToken(tmp, TokenTypes.NUMBER);
                    return;
                }
            }
        }

        private lexString() {
            var tmp: string = "";
            var closeFound: boolean = false;
            this.move();
            while (this.hasMoreChars()) {
                if (this.getCharType(this.peek()) != charTypes.EOL && this.peek() != '"') {
                    tmp += this.read();
                }
                else if (this.peek() == '"') {
                    this.move();
                    this.acceptToken(tmp, TokenTypes.STRING);
                    return;
                }
                else if (this.getCharType(this.peek()) == charTypes.EOL) {
                    this.acceptToken(tmp, TokenTypes.STRING, "EOL was found");//ERROR
                    return;
                }
            }
            this.acceptToken(tmp, TokenTypes.STRING, "EOF was found");
        }

        initReserved() {
            this.reservedMap["if"] = TokenTypes.IF;
            this.reservedMap["else"] = TokenTypes.ELSE;
            this.reservedMap["end"] = TokenTypes.END;
            this.reservedMap["then"] = TokenTypes.THEN;
            this.reservedMap["draw"] = TokenTypes.DRAW;
            this.reservedMap["circle"] = TokenTypes.CIRCLE;
            this.reservedMap["rectangle"] = TokenTypes.RECTANGLE;
            this.reservedMap["text"] = TokenTypes.TEXT;
            this.reservedMap["line"] = TokenTypes.LINE;
            this.reservedMap["rect"] = TokenTypes.RECTANGLE;
            this.reservedMap["with"] = TokenTypes.WITH;
            this.reservedMap["if"] = TokenTypes.IF;
            this.reservedMap["at"] = TokenTypes.AT;
            this.reservedMap["from"] = TokenTypes.FROM;
            this.reservedMap["if"] = TokenTypes.IF;
            this.reservedMap["while"] = TokenTypes.WHILE;
            this.reservedMap["for"] = TokenTypes.FOR;
            this.reservedMap["in"] = TokenTypes.IN;
            this.reservedMap["do"] = TokenTypes.DO;
            this.reservedMap["to"] = TokenTypes.TO;
            this.reservedMap["let"] = TokenTypes.LET;
            this.reservedMap["be"] = TokenTypes.BE;
            this.reservedMap["if"] = TokenTypes.IF;
            this.reservedMap["and"] = TokenTypes.AND;
            this.reservedMap["or"] = TokenTypes.OR;
            this.reservedMap["not"] = TokenTypes.NOT;
            this.reservedMap["("] = TokenTypes.L_PARA;
            this.reservedMap[")"] = TokenTypes.R_PARA;
            this.reservedMap[","] = TokenTypes.COMMA;
            this.reservedMap["="] = TokenTypes.EQUAL;
            this.reservedMap["+"] = TokenTypes.PLUS;
            this.reservedMap["-"] = TokenTypes.MINUS;
            this.reservedMap["*"] = TokenTypes.MUL;
            this.reservedMap["/"] = TokenTypes.DIV;
            this.reservedMap["%"] = TokenTypes.MOD;
            this.reservedMap["^"] = TokenTypes.POW;
        }

        reserved(lexeme:string) {
            return this.reservedMap[lexeme];
        }

        private lexId() {
            var c: string;
            var cType: charTypes;
            var tmp: string = "";
            var first: boolean = true;
            while (this.hasMoreChars()) {
                c = this.peek();
                cType = this.getCharType(c);
                if (cType == charTypes.LETTER || c == '_') {
                    tmp += this.read();
                }
                else if (cType == charTypes.NUM && !first) {
                    tmp += this.read();
                }
                else {
                    var tokenType:TokenTypes = this.reserved(tmp);
                    if (tokenType == undefined) {
                        this.acceptToken(tmp, TokenTypes.ID);
                    }
                    else if (tokenType == TokenTypes.NOT) {
                        if (this.peek() == '=') {
                            this.move();
                            this.acceptToken("not=", TokenTypes.NOT_EQUAL);
                        }
                        else {
                            this.acceptToken(tmp, tokenType);
                        }
                    }
                    else {
                        this.acceptToken(tmp, tokenType);
                    }
                    return;
                }
                first = false;
            }
        }

        private lexSymbol() {
            var c: string;
            c = this.read();
            if (c == '>') {
                if (this.hasMoreChars() && this.peek() == '=') {
                    this.move();
                    this.acceptToken(">=", TokenTypes.GREATER_EQUAL);
                }
                else {
                    this.acceptToken(">", TokenTypes.GREATER);
                }
            }
            else if (c == '<') {
                if (this.hasMoreChars() && this.peek() == '=') {
                    this.move();
                    this.acceptToken("<=", TokenTypes.SMALLER_EQUAL);
                }
                else {
                    this.acceptToken("<", TokenTypes.SMALLER);
                }
            }
            else {
                var tokenType:TokenTypes = this.reserved(c);
                if (tokenType != undefined) {
                    this.acceptToken(c, tokenType);
                }
                else {
                    this.acceptToken(c, TokenTypes.INVALID);//ERROR
                }
            }
        }
        
        lexEOL() {
            this.acceptToken(this.read(), TokenTypes.EOL);
            this.lineNum++;
            this.colNum = 0;
            while (this.getCharType(this.peek()) == charTypes.EOL) {
                this.lineNum++;
                this.move();
            }
            var cDepth: number = 0;
            while (this.peek() == '-') {
                cDepth++;
                this.move();
            }
            if (cDepth == this.depth + 1) {
                this.acceptToken("~+", TokenTypes.INDENT);
                this.depth++;
            }
            else if (cDepth < this.depth) {
                for (var i = 0; i < this.depth - cDepth; i++) {
                    this.acceptToken("~-", TokenTypes.DEDENT);
                }
                this.depth = cDepth;
            }
        }
        //END Lex Tools



        //Main
        public run() {
            this.reset();
            var tmp: string = "";
            this.Tokens.length = 0;
            while (this.hasMoreChars()) {
                var c: string;
                var cType: charTypes;
                c = this.peek();
                cType = this.getCharType(c);
                if (cType == charTypes.NUM) {
                    this.lexNum();
                }
                else if (c == '"') {
                    this.lexString();
                }
                else if (cType == charTypes.LETTER) {
                    this.lexId();
                }
                else if (cType == charTypes.DELI || cType == charTypes.OP) {
                    this.lexSymbol();
                }
                else if (cType == charTypes.SKIP) {
                    this.move();
                }
                else if (cType == charTypes.EOL) {
                    this.lexEOL();
                }
                else {
                    this.acceptToken(this.read(), TokenTypes.INVALID, "Unexpected");//ERROR
                }
            }
            return this.Tokens;
        }

    }


}