///<reference path="jquery.d.ts"/>
///<reference path="ace-src-noconflict/ace.d.ts"/>
///<reference path="Language.ts"/>

module EditorModule {
    export class Editor {
        private lastStepsList: Language.Step[];

        private aceEditor: AceAjax.Editor;
        private aceRowCount: number = 1;
        private acePrevLength: number = 0;

        private tableColCount: number = 0;
        private tableRowCount: number = 0;
        private lastHoveredRow: number = 0;
        private lastHoveredCol: number = 0;

        private livePreview: boolean = true;
        private liveDebug: boolean = true;

        private canv: HTMLCanvasElement;

        langManager: Language.LangManager;

        constructor(
            public editorID: string, public tableDivID: string,
            public tableScrollDivID: string, public canvasID: string,
            public livePreviewCheckBoxID: string= "", public runButtonID: string= "",
            public liveDebugCheckBoxID: string= "", public canvasMousPosDivID: string= ""
            ) {
            var cthis = this;

            this.canv = <HTMLCanvasElement> document.getElementById(canvasID);
            this.langManager = new Language.LangManager(this.canv);

            //#region aceEditor
            this.aceEditor = ace.edit(editorID);
            this.aceEditor.setTheme("ace/theme/dreamweaver");
            this.aceEditor.getSession().setMode("ace/mode/python");
            this.aceEditor.resize();

            this.aceEditor.getSession().selection.on("changeCursor", function () { cthis.aceEditorOnChangeCursor(); });
            this.aceEditor.getSession().on("change", function () { cthis.aceEditorOnChange() });
            this.aceEditor.getSession().on("changeScrollTop", function () { cthis.aceEditorOnChangeScrollTop() });

            ////#endregion 

            //#region Table
            this.tableDivID = '#' + this.tableDivID;

            $(this.tableDivID).delegate(".divCell", "mouseover mouseleave", function (e) { cthis.onCellHover(e, this) });
            //#endregion 

            //#region Control Panel
            if (this.livePreviewCheckBoxID != "") {
                this.livePreviewCheckBoxID = '#' + this.livePreviewCheckBoxID;
            }
            $(this.livePreviewCheckBoxID).change(function () { cthis.livePreviewOnChange() });

            if (this.runButtonID != "") {
                this.runButtonID = '#' + this.runButtonID;
            }
            $(this.runButtonID).click(function () { cthis.onRunButtonClick() });


            if (this.liveDebugCheckBoxID != "") {
                this.liveDebugCheckBoxID = '#' + this.liveDebugCheckBoxID;
            }
            $(this.liveDebugCheckBoxID).change(function () { cthis.liveDebugOnChange() });


            if (this.canvasMousPosDivID != "") {
                this.canvasMousPosDivID = '#' + this.canvasMousPosDivID;
                $('#' + this.canvasID).mousemove(function (e) { cthis.onCanvasMouseMove(e) });
            }
            //#endregion 

            this.lastStepsList = new Array();
        }

        runCode(run: boolean= true) {
            if (!run) {
                return;
            }
            var newCode = this.aceEditor.getSession().getValue();
            this.langManager.updateCode(newCode);
            var tempStepsList: Language.Step[];
            tempStepsList = this.langManager.run();
            if (this.liveDebug && tempStepsList.length <= 10000)
                this.drawStepsList(tempStepsList);
            else {
                this.drawStepsList(new Array());
            }
        }

        //#region stepsList

        cleanLastStepsList() {
            if (this.lastStepsList == null) return;
            for (var i = 0; i < this.lastStepsList.length; i++) {
                if (this.lastStepsList[i].lineNum > this.tableRowCount) continue;
                $(this.tableDivID).children().eq(this.lastStepsList[i].lineNum).children().eq(i).text('');
            }
        }

        drawStepsList(stepsList: Language.Step[]) {
            this.cleanLastStepsList();
            this.resizeTable(-1, stepsList.length);
            for (var i = 0; i < stepsList.length; i++) {
                $(this.tableDivID).children().eq(stepsList[i].lineNum).children().eq(i).text('\u2022');
            }
            this.lastStepsList = stepsList;
        }

        //#endregion 



        //#region EventHandling

        onCanvasMouseMove(e) {
            var mouseX, mouseY;


            mouseX = e.offsetX;
            mouseY = e.offsetY;


            if (mouseX == undefined) {
                var targ = e.target;
                mouseX = e.pageX - $(targ).offset().left - 0.5;
                mouseY = e.pageY - $(targ).offset().top;
            }

            $(this.canvasMousPosDivID).text("(" + mouseX + "," + mouseY + ")");
        }

        //#region ControlPanel
        livePreviewOnChange() {
            if ($(this.livePreviewCheckBoxID).is(':checked')) {
                this.livePreview = true;
                this.aceEditorOnChange();
                $(this.runButtonID).prop("disabled", true);
            }
            else {
                this.acePrevLength = 0;
                this.resizeTable(0, 0);
                this.livePreview = false;
                $(this.runButtonID).prop("disabled", false);
            }
        }

        liveDebugOnChange() {
            this.liveDebug = ($(this.liveDebugCheckBoxID).is(':checked'));
        }

        onRunButtonClick() {
            this.aceEditorOnChange();
            this.runCode();
        }

        //#endregion

        //#region aceEditor
        aceEditorOnChangeScrollTop() {
            document.getElementById(this.tableScrollDivID).scrollTop = this.aceEditor.session.getScrollTop();
        }

        aceEditorOnChange() {
            // handle length change event
            var curLength = this.aceEditor.getSession().getLength();
            if (curLength != this.acePrevLength) {
                this.aceEditorOnLengthChange(curLength);
                this.acePrevLength = curLength;
            }
            //
            this.runCode(this.livePreview);
        }

        aceEditorOnChangeCursor() {
            var x = this.aceEditor.getSession().getLength();
            this.tableHoverRow();
        }

        aceEditorOnLengthChange(newLength: number) {
            this.resizeTable(newLength, -1);
        }

        //#endregion 


        //#region table

        onCellHover(e: Event, ethis: any) {
            var hoveredCol = $(ethis).index();
            var hoveredRow = $(ethis).parent().index();

            this.aceEditor.gotoLine(this.lastStepsList[hoveredCol].lineNum + 1);

            this.onColHover(hoveredCol, e);
        }

        onColHover(hoveredCol: number, e: Event) {
            $(this.tableDivID).children().each(function (index, value) {
                if (e.type == "mouseover") {
                    $(value).children().eq(hoveredCol).addClass("hover");
                }
                else {
                    $(value).children().eq(hoveredCol).removeClass("hover");
                }
            })
            this.langManager.interpret(hoveredCol + 1);
        }

        tableHoverRow() {
            var hoveredRow = this.aceEditor.getSession().selection.getCursor().row;
            $(this.tableDivID).children().eq(this.lastHoveredRow).removeClass("hover-row");
            $(this.tableDivID).children().eq(hoveredRow).addClass("hover-row");
            this.lastHoveredRow = hoveredRow;
        }

        //#endregion

        //#endregion
        //#region tableResize
        tableAddRow() {
            this.tableRowCount++;
            $(this.tableDivID).append('<div class="divRow"></div>');
            for (var i = 0; i < this.tableColCount; i++) {
                $(this.tableDivID).children().last().append('<div class="divCell"></div>');
            }
        }

        tableRemoveRow() {
            this.tableRowCount--;
            $(this.tableDivID).children().last().remove();
        }

        tableAddCol() {
            this.tableColCount++;
            $(this.tableDivID).children().each(function (index, value) {
                $(value).append('<div class="divCell"></div>');
            })
        }

        tableRemoveCol() {
            this.tableColCount--;
            $(this.tableDivID).children().each(function (index, value) {
                $(value).children().last().remove();
            })
        }

        resizeTable(rows: number, cols: number) {
            var curRows = this.tableRowCount;
            var curCols = this.tableColCount;
            if (rows == -1) rows = curRows;
            if (cols == -1) cols = curCols;
            if (curRows < rows) {
                for (var i = 0; i < rows - curRows; i++) {
                    this.tableAddRow();
                }
            }
            else if (curRows > rows) {
                for (var i = 0; i < curRows - rows; i++) {
                    this.tableRemoveRow();
                }
            }

            if (curCols < cols) {
                for (var i = 0; i < cols - curCols; i++) {
                    this.tableAddCol();
                }
            }
            else if (curCols > cols) {
                for (var i = 0; i < curCols - cols; i++) {
                    this.tableRemoveCol();
                }
            }
        }
        //#endregion
    }

}