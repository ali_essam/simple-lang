grammar Grammar;

stat_list
    :     (stat)+ 
    ;

stat
    :    (simple_stat|compound_stat)|EOL
    ;

simple_stat
    :    (draw_stat|let_stat) EOL
    ;
compound_stat
    :    if_stat
    ;

if_stat
    :   IF expression THEN EOL INDENT stat_list DEDENT
        (ELSE IF expression THEN EOL INDENT stat_list DEDENT)*
        (ELSE EOL INDENT stat_list DEDENT)?
    ;

while_stat
    :   WHILE expression EOL INDENT stat_list DEDENT
    ;

for_stat
    :   FOR ID IN expression EOL INDENT stat_list DEDENT
    ;

let_stat
    :    LET ID BE expression
    ;
draw_stat
    :    DRAW (draw_circle|draw_rect) ( WITH (key_value)+ )?
    ;
draw_circle
    :    CIRCLE AT point
    ;
draw_rect 
    :    RECTANGLE FROM point TO point
    ;
draw_text
    :    TEXT expression AT point
    ;

key_value
    :    ID EQUAL (expression|point)
    ;
point
    :    L_PARA expression COMMA expression R_PARA
    ;

expression
    :    logical_expr
    ;

logical_expr
    :   comp_expr ((AND|OR) comp_expr)*
    ;
comp_expr  
    :   add_expr
        ((EQUAL|NOT_EQUAL|GREATER|GREATER_EQUAL|SMALLER|SMALLER_EQUAL)
        add_expr)*
    ;
add_expr
    :   mul_expr((PLUS|MINUS) mul_expr)*
    ;
mul_expr
    :   pow_expr ((MUL|DIV|MOD) pow_expr)*
    ;
pow_expr
    :   factor(POW pow_expr)*
    ;
factor
    :   function_call
    |   (NUMBER|ID|STRING)
    |   (L_PARA expression R_PARA)
    |   MINUS factor
    |   NOT factor
    ;

function_call
    :   ID L_PARA parameter_list R_PARA
    ;

parameter_list
    :(expression (COMMA expression)*)?
    ;

INDENT:'~+';//for testing purpose only
DEDENT:'~-';

L_PARA:'(';
R_PARA:')';
COMMA:',';

NOT_EQUAL:'not=';
EQUAL:'=';
GREATER:'>';
GREATER_EQUAL:'>=';
SMALLER:'<';
SMALLER_EQUAL:'<=';

PLUS:'+';
MINUS:'-';

MUL:'*';
DIV:'/';
MOD:'%';
POW:'^';

IF:'if';
ELSE:'else';
END:'end';
THEN:'then';
DRAW:'draw';
CIRCLE:'circle';
RECTANGLE:'rectangle'|'rect';
WITH:'with';
AT:'at';
FROM:'from';
TO:'to';
LET:'let';
BE:'be';

AND:'and';
OR:'or';
NOT:'not';

EOL:('\r'? '\n');

NUMBER: ('0'..'9')+ ('.' ('0'..'9')*)?;
ID:   ('A'..'Z'|'a'..'z'|'_') ('A'..'Z'|'a'..'z'|'_'|'0'..'9')*;
STRING : '"' (.)*? '"' ;
WS:(' '|'\t')+->skip;