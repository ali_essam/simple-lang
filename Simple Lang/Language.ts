/// <reference path="LangParser.ts"/>
/// <reference path="LangLexer.ts"/>
/// <reference path="LangInterpreter.ts"/>
/// <reference path="LangObjects.ts"/>
/// <reference path="IOManager.ts"/>
// Module

module Language {

    // Class

    export class Token {
        constructor(public lexeme, public type, public pos, public line, public col, public error) { }
        isType(_type: TokenTypes) {
            return this.type == _type;
        }
        isLexeme(_lexeme: string) {
            return this.lexeme == _lexeme;
        }
    }

    export class Step {
        constructor(public lineNum: number) { }
    }


    export class LangManager {
        code: string;
        private lexer: Lexer;
        private parser: Parser;
        private interpreter: Interpreter;
        private ioManager: IOManager;
        private root: Node;
        constructor(canvas:HTMLCanvasElement) {
            this.lexer = new Lexer();
            this.parser = new Parser(); 
            this.ioManager = new IOManager(canvas);
            this.interpreter = new Interpreter(this.ioManager);
        }

        public updateCode(_code: string) {//TODO
            this.code = _code;
            this.lexer.updateCode(_code);
        }
        lex(){
            return this.lexer.run();
        }
        parse() {
            return this.parser.parse();
        }

        interpret(maxSteps: number) {
            this.interpreter.Interpret(this.root,maxSteps);
        }

        public run():Step[] {
            console.clear()
            var Tokens: Token[] = this.lex();
            for (var x = 0; x < Tokens.length; x++) {
                //console.log(Tokens[x].lexeme + " pos:" + Tokens[x].pos + " type:" + Tokens[x].type );
                console.log(Tokens[x]);
            }
            this.parser.updateTokenList(Tokens);
            this.root = this.parse();
            console.log(this.root);
            if (!this.parser.hasErrors) {
                this.interpreter.Interpret(this.root);
                console.log(this.interpreter.varTable);
                return this.interpreter.getStepsList();
            }
            console.error("Parse Error");
            return new Array();
        }
    }
}