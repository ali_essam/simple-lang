/// <reference path="Language.ts"/>

module Language {
    export class Interpreter {

        varTable: LangObject[];
        stepsList: Step[];
        maxStepsLength: number;

        constructor(public ioManager: IOManager) {
            this.varTable = new Array();
            this.stepsList = new Array();
            this.maxStepsLength = Infinity;
        }

        Interpret(root: Node, _maxStepsLength: number = Infinity) {
            this.maxStepsLength = _maxStepsLength;
            this.varTable = new Array();
            this.stepsList = new Array();
            this.ioManager.resetCanvas();
            this.statListInterpret(<StatementList>root);
        }

        addStep(token: Token) {
            this.stepsList.push(new Step(token.line));
        }

        getStepsList(): Step[] {
            return this.stepsList;
        }

        statListInterpret(statList: StatementList) {
            for (var i = 0; i < statList.statements.length; i++) {
                this.statInterpret(statList.statements[i]);
                if (this.stepsList.length >= this.maxStepsLength) break;
            }
        }

        statInterpret(stat: Statement) {
            switch (stat.type) {
                case StatementTypes.BIND:
                    this.bindInterpret(<BindStatement>stat);
                    break;
                case StatementTypes.DRAW:
                    this.drawInterpret(<DrawStat>stat);
                    break;
                case StatementTypes.IF:
                    this.ifInterpret(<IfStatement>stat);
                    break;
                case StatementTypes.WHILE:
                    this.whileInterpret(<WhileStatement>stat);
                    break;
                case StatementTypes.FOR:
                    this.forInterpret(<ForStatement>stat);
                    break;
                case StatementTypes.STAT_LIST:
                    this.statListInterpret((<StatementList>stat));
                    break;
            }
        }

        ifInterpret(stat: IfStatement) {
            var data: IfData;
            var cond: NumberObject;
            for (var i = 0; i < stat.data.length; i++) {
                data = stat.data[i];
                this.addStep(data.condition.token);
                cond = <NumberObject> this.expressionInterpret(data.condition);
                if (NumberObject.toBool(cond)) {
                    this.statListInterpret(data.body);
                    break;
                }
            }
        }

        whileInterpret(stat: WhileStatement) {
            var numberOfLoops = 0;
            while (NumberObject.toBool(<NumberObject> this.expressionInterpret(stat.condition)) && numberOfLoops < 100) {
                this.addStep(stat.token);
                if (this.stepsList.length >= this.maxStepsLength) break;
                this.statListInterpret(stat.body);
                numberOfLoops++;
            }
        }

        forInterpret(stat: ForStatement) {
            var iterableObject = this.expressionInterpret(stat.iterable);
            if (iterableObject.isIterable) {
                console.log("FOR");
                iterableObject.initIterator();
                while (iterableObject.hasMoreItems()) {
                    this.addStep(stat.token);
                    if (this.stepsList.length >= this.maxStepsLength) break;

                    this.varTable[stat.id] = iterableObject.nextItem();
                    console.info(this.varTable[stat.id]);
                    this.statListInterpret(stat.body);
                }
            }
        }

        drawInterpret(stat: DrawStat) {
            switch (stat.drawType) {
                case DrawTypes.CIRCLE:
                    this.addStep(stat.token);
                    return this.drawCircleInterpret(<DrawCircleStat>stat);
                case DrawTypes.RECT:
                    this.addStep(stat.token);
                    return this.drawRectInterpret(<DrawRectStat>stat);
                case DrawTypes.LINE:
                    this.addStep(stat.token);
                    return this.drawLineInterpret(<DrawLineStat>stat);
                case DrawTypes.TEXT:
                    this.addStep(stat.token);
                    return this.drawTextInterpret(<DrawTextStat>stat);
            }
        }

        drawCircleInterpret(stat: DrawCircleStat) {
            var _x: LangObject = this.expressionInterpret(stat.center.x);
            var _y: LangObject = this.expressionInterpret(stat.center.y);
            if (_x.type != ObjectTypes.NUMBER || _y.type != ObjectTypes.NUMBER) {
                console.error("Number Expected");//ERROR
                return null;
            }
            var x = <NumberObject>_x;
            var y = <NumberObject>_y;
            var radius: NumberObject = new NumberObject(10);
            var opacity: NumberObject = new NumberObject(100);
            var color: StringObject = new StringObject("red");
            if (stat.keyValues["radius"] != undefined) {
                radius = <NumberObject>(this.expressionInterpret(stat.keyValues["radius"]));
            }
            if (stat.keyValues["color"] != undefined) {
                color = <StringObject>(this.expressionInterpret(stat.keyValues["color"]));
            }
            if (stat.keyValues["opacity"] != undefined) {
                opacity = <NumberObject>(this.expressionInterpret(stat.keyValues["opacity"]));
            }
            this.ioManager.drawCircle(x.value, y.value, radius.value, color.value, opacity.value);
        }

        drawTextInterpret(stat: DrawTextStat) {
            var _x: LangObject = this.expressionInterpret(stat.center.x);
            var _y: LangObject = this.expressionInterpret(stat.center.y);
            if (_x.type != ObjectTypes.NUMBER || _y.type != ObjectTypes.NUMBER) {
                console.error("Number Expected");//ERROR
                return null;
            }
            var x = <NumberObject>_x;
            var y = <NumberObject>_y;
            var value = this.expressionInterpret(stat.value);
            var opacity: NumberObject = new NumberObject(100);
            var color: StringObject = new StringObject("red");

            var size: NumberObject = new NumberObject(10);
            var font: StringObject = new StringObject("Arial");
            if (stat.keyValues["color"] != undefined) {
                color = <StringObject>(this.expressionInterpret(stat.keyValues["color"]));
            }
            if (stat.keyValues["opacity"] != undefined) {
                opacity = <NumberObject>(this.expressionInterpret(stat.keyValues["opacity"]));
            }
            if (stat.keyValues["size"] != undefined) {
                size = <NumberObject>(this.expressionInterpret(stat.keyValues["size"]));
            }
            if (stat.keyValues["font"] != undefined) {
                font = <StringObject>(this.expressionInterpret(stat.keyValues["font"]));
            }
            this.ioManager.drawText(x.value, y.value, value.value, color.value, opacity.value, size.value,font.value);
        }

        drawRectInterpret(stat: DrawRectStat) {
            var _x1: LangObject = this.expressionInterpret(stat.from.x);
            var _y1: LangObject = this.expressionInterpret(stat.from.y);
            var _x2: LangObject = this.expressionInterpret(stat.to.x);
            var _y2: LangObject = this.expressionInterpret(stat.to.y);
            if (_x1.type != ObjectTypes.NUMBER || _y1.type != ObjectTypes.NUMBER || _x2.type != ObjectTypes.NUMBER || _y2.type != ObjectTypes.NUMBER) {
                console.error("Number Expected");//ERROR
                return null;
            }
            var x1 = <NumberObject>_x1;
            var y1 = <NumberObject>_y1;
            var x2 = <NumberObject>_x2;
            var y2 = <NumberObject>_y2;
            var opacity: NumberObject = new NumberObject(100);
            var color: StringObject = new StringObject("red");
            if (stat.keyValues["color"] != undefined) {
                color = <StringObject>(this.expressionInterpret(stat.keyValues["color"]));
            }
            if (stat.keyValues["opacity"] != undefined) {
                opacity = <NumberObject>(this.expressionInterpret(stat.keyValues["opacity"]));
            }
            this.ioManager.drawRect(x1.value, y1.value, x2.value, y2.value, color.value, opacity.value);
        }

        drawLineInterpret(stat: DrawLineStat) {
            var _x1: LangObject = this.expressionInterpret(stat.from.x);
            var _y1: LangObject = this.expressionInterpret(stat.from.y);
            var _x2: LangObject = this.expressionInterpret(stat.to.x);
            var _y2: LangObject = this.expressionInterpret(stat.to.y);
            if (_x1.type != ObjectTypes.NUMBER || _y1.type != ObjectTypes.NUMBER || _x2.type != ObjectTypes.NUMBER || _y2.type != ObjectTypes.NUMBER) {
                console.error("Number Expected");//ERROR
                return null;
            }
            var x1 = <NumberObject>_x1;
            var y1 = <NumberObject>_y1;
            var x2 = <NumberObject>_x2;
            var y2 = <NumberObject>_y2;
            var opacity: NumberObject = new NumberObject(100);
            var size: NumberObject = new NumberObject(1);
            var color: StringObject = new StringObject("red");
            if (stat.keyValues["color"] != undefined) {
                color = <StringObject>(this.expressionInterpret(stat.keyValues["color"]));
            }
            if (stat.keyValues["opacity"] != undefined) {
                opacity = <NumberObject>(this.expressionInterpret(stat.keyValues["opacity"]));
            }
            if (stat.keyValues["size"] != undefined) {
                size = <NumberObject>(this.expressionInterpret(stat.keyValues["size"]));
            }
            this.ioManager.drawLine(x1.value, y1.value, x2.value, y2.value, size.value, color.value, opacity.value);
        }

        bindInterpret(stat: BindStatement) {
            this.addStep(stat.token);
            this.varTable[stat.identifier] = this.expressionInterpret(stat.value);
        }

        expressionInterpret(node: Node): LangObject {
            switch (node.nodeType) {
                case NodeTypes.NUM_LITERAL:
                    return this.numInterpret(<NumLiteral>node);
                case NodeTypes.STRING_LITERAL:
                    return this.stringInterpret(<StringLiteral>node);
                case NodeTypes.ID:
                    return this.idInterpret(<Identifier>node);
                case NodeTypes.BINARY_OPERATOR:
                    return this.binaryOpInterpret(<BinaryOperator>node);
                case NodeTypes.UNARY_OPERATOR:
                    return this.unaryOpInterpret(<UnaryOperator>node);
                case NodeTypes.FUNCTION_CALL:
                    return this.functionCallInterpret(<FunctionCall>node);
            }
        }



        //Operator Interpret
        unaryOpInterpret(node: UnaryOperator): LangObject {
            switch (node.operator) {
                case OperatorTypes.NOT:
                    return this.notInterpret(node);
                case OperatorTypes.NEGATIVE:
                    return this.negativeInterpret(node);
            }
        }

        notInterpret(node: UnaryOperator): LangObject {
            var op = this.expressionInterpret(node.operand);
            return op.not();
        }
        negativeInterpret(node: UnaryOperator): LangObject {
            var op = this.expressionInterpret(node.operand);
            return op.negative();
        }


        binaryOpInterpret(node: BinaryOperator): LangObject {
            switch (node.operator) {
                case OperatorTypes.ADD:
                    return this.addInterpret(node);
                case OperatorTypes.SUB:
                    return this.subInterpret(node);
                case OperatorTypes.MUL:
                    return this.mulInterpret(node);
                case OperatorTypes.MOD:
                    return this.modInterpret(node);
                case OperatorTypes.DIV:
                    return this.divInterpret(node);
                case OperatorTypes.POW:
                    return this.powInterpret(node);
                case OperatorTypes.EQUALS:
                    return this.equalInterpret(node);

                case OperatorTypes.AND:
                    return this.andInterpret(node);
                case OperatorTypes.OR:
                    return this.orInterpret(node);

                case OperatorTypes.EQUALS:
                    return this.equalInterpret(node);
                case OperatorTypes.NOT_EQUALS:
                    return this.notEqualInterpret(node);
                case OperatorTypes.GREATER:
                    return this.greaterInterpret(node);
                case OperatorTypes.GREATER_EQUAL:
                    return this.greaterEqualInterpret(node);
                case OperatorTypes.SMALLER:
                    return this.smallerInterpret(node);
                case OperatorTypes.SMALLER_EQUAL:
                    return this.smallerEqualInterpret(node);

            }
        }
        //Logical
        andInterpret(node: BinaryOperator): LangObject {
            var op1 = this.expressionInterpret(node.operand1);
            var op2 = this.expressionInterpret(node.operand2);
            return NumberObject.fromBool(op1.and(op2));
        }

        orInterpret(node: BinaryOperator): LangObject {
            var op1 = this.expressionInterpret(node.operand1);
            var op2 = this.expressionInterpret(node.operand2);
            return NumberObject.fromBool(op1.or(op2));
        }

        //Comparison

        equalInterpret(node: BinaryOperator): LangObject {
            var op1 = this.expressionInterpret(node.operand1);
            var op2 = this.expressionInterpret(node.operand2);
            return NumberObject.fromBool(op1.equal(op2));
        }

        notEqualInterpret(node: BinaryOperator): LangObject {
            var op1 = this.expressionInterpret(node.operand1);
            var op2 = this.expressionInterpret(node.operand2);
            return NumberObject.fromBool(op1.notEqual(op2));
        }

        greaterInterpret(node: BinaryOperator): LangObject {
            var op1 = this.expressionInterpret(node.operand1);
            var op2 = this.expressionInterpret(node.operand2);
            return NumberObject.fromBool(op1.greater(op2));
        }

        greaterEqualInterpret(node: BinaryOperator): LangObject {
            var op1 = this.expressionInterpret(node.operand1);
            var op2 = this.expressionInterpret(node.operand2);
            return NumberObject.fromBool(op1.greaterEqual(op2));
        }

        smallerInterpret(node: BinaryOperator): LangObject {
            var op1 = this.expressionInterpret(node.operand1);
            var op2 = this.expressionInterpret(node.operand2);
            return NumberObject.fromBool(op1.smaller(op2));
        }

        smallerEqualInterpret(node: BinaryOperator): LangObject {
            var op1 = this.expressionInterpret(node.operand1);
            var op2 = this.expressionInterpret(node.operand2);
            return NumberObject.fromBool(op1.smallerEqual(op2));
        }

        //Math

        addInterpret(node: BinaryOperator): LangObject {
            var op1 = this.expressionInterpret(node.operand1);
            var op2 = this.expressionInterpret(node.operand2);
            return op1.add(op2);
        }

        mulInterpret(node: BinaryOperator): LangObject {
            var op1 = this.expressionInterpret(node.operand1);
            var op2 = this.expressionInterpret(node.operand2);
            return op1.mul(op2);
        }

        subInterpret(node: BinaryOperator): LangObject {
            var op1 = this.expressionInterpret(node.operand1);
            var op2 = this.expressionInterpret(node.operand2);
            return op1.sub(op2);
        }

        divInterpret(node: BinaryOperator): LangObject {
            var op1 = this.expressionInterpret(node.operand1);
            var op2 = this.expressionInterpret(node.operand2);
            return op1.div(op2);
        }

        modInterpret(node: BinaryOperator): LangObject {
            var op1 = this.expressionInterpret(node.operand1);
            var op2 = this.expressionInterpret(node.operand2);
            return op1.mod(op2);
        }

        powInterpret(node: BinaryOperator): LangObject {
            var op1 = this.expressionInterpret(node.operand1);
            var op2 = this.expressionInterpret(node.operand2);
            return op1.pow(op2);
        }

        //End Operator Interpret


        // Factor Interpret
        numInterpret(node: NumLiteral): NumberObject {
            return new NumberObject(node.value);
        }

        stringInterpret(node: StringLiteral): StringObject {
            return new StringObject(node.value);
        }

        idInterpret(node: Identifier): LangObject {
            var val: LangObject = this.varTable[node.name];
            if (val != undefined) {
                if (val.type == ObjectTypes.NUMBER) {
                    return new NumberObject((<NumberObject>val).value);
                }
                else if (val.type == ObjectTypes.STRING) {
                    return new StringObject((<StringObject>val).value);
                }
            }
            else {
                return new NumberObject(0);
            }
        }

        functionCallInterpret(node: FunctionCall): LangObject {
            switch (node.id) {
                case "range":
                    return this.builtInFunctionRange(node);
                case "rgb":
                    return this.builtInFunctionRGB(node);
            }
        }

        builtInFunctionRange(node: FunctionCall): ArrayObject {
            var rangeList: LangObject[] = new Array();
            var one: NumberObject = new NumberObject(1);
            var step: LangObject = one;
            var i: LangObject;
            var end: LangObject;

            if (node.parameterList.length == 2) {
                i = this.expressionInterpret(node.parameterList[0]);
                end = this.expressionInterpret(node.parameterList[1]);
            }
            else if (node.parameterList.length == 1) {
                i = one;
                end = this.expressionInterpret(node.parameterList[0]);
            }
            else if (node.parameterList.length == 3) {
                i = this.expressionInterpret(node.parameterList[0]);
                end = this.expressionInterpret(node.parameterList[1]);
                step = this.expressionInterpret(node.parameterList[2]);
            }
            else {
                console.error("Invalid parameter list");//ERROR
            }
            for (; i.smallerEqual(end); i = i.add(step)) {
                rangeList.push(i);
            }
            return new ArrayObject(rangeList);
        }
        builtInFunctionRGB(node: FunctionCall): StringObject {
            var p = node.parameterList;
            if (p.length != 3) {
                console.error("rgb wrong parameter list");
                return new StringObject("black");
            }
            var r = this.expressionInterpret(p[0]);
            var g = this.expressionInterpret(p[1]);
            var b = this.expressionInterpret(p[2]);
            return new StringObject("rgb(" + r.value + "," + g.value + "," + b.value + ")");
        }
        // End Factor Interpret
    }


}