// Module
module Language {

    export class IOManager {
        ctx:CanvasRenderingContext2D;
        constructor(public canvasElement: HTMLCanvasElement) {
            this.ctx = this.canvasElement.getContext("2d");
            this.resetCanvas();
        }

        resetCanvas() {
            this.ctx.clearRect(0, 0, this.ctx.canvas.width, this.ctx.canvas.height);
        }

        drawCircle(x: number, y: number, radius: number, color: string,opacity:number) {
            this.ctx.beginPath();
            this.ctx.globalAlpha = opacity/100;
            this.ctx.arc(x, y, radius, 0, 2 * Math.PI);
            this.ctx.fillStyle = color;
            this.ctx.closePath();
            this.ctx.fill();
        }

        drawText(x: number, y: number, value: string, color: string, opacity: number,size:number,font:string) {
            this.ctx.globalAlpha = opacity / 100;//TODO
            var tst = (size.toString()) + 'px' + ' ' + font;
            this.ctx.font = tst;
            this.ctx.fillStyle = color;
            this.ctx.fillText(value, x, y);
        }

        drawRect(x1: number, y1: number, x2: number, y2: number, color: string, opacity: number) {
            this.ctx.beginPath();
            this.ctx.globalAlpha = opacity / 100;
            this.ctx.rect(x1, y1, x2 - x1, y2 - y1);
            this.ctx.fillStyle = color;
            this.ctx.closePath();
            this.ctx.fill();
        }

        drawLine(x1: number, y1: number, x2: number, y2: number,size:number, color: string, opacity: number) {
            this.ctx.beginPath();
            this.ctx.globalAlpha = opacity / 100;
            this.ctx.moveTo(x1, y1);
            this.ctx.lineTo(x2, y2);
            this.ctx.strokeStyle = color;
            this.ctx.closePath();
            this.ctx.lineWidth = size;
            this.ctx.stroke();
        }

    }
    
    

}