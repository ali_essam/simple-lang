/// <reference path="ace-src-noconflict/ace.d.ts"/>
/// <reference path="jquery.d.ts"/>
/// <reference path="Editor.ts"/>

class editorGenerator {

    static generateEditor(divID: string,controlPanel:boolean=true,editorHeight:number=0,canvasHeight:number=130,canvasWidth:number=530) {
        var code = $('#' + divID).html();
        if (editorHeight == 0) {
            editorHeight = code.split('\n').length * 18;
        }
        if (editorHeight < 50) editorHeight = 50;
        var controlPanelDisplay = (controlPanel) ? 'inline-block' : 'none';

        var html: string ='\
        <div class="row" style="margin-left: 0px;">\
            <pre id = "editor-' + divID + '" style = "height: '+editorHeight+'px; float: left; display: block; padding: 9.5px 0px 9.5px 0px; width: 49%;line-height:14px;" >\n'+code+'\n</pre>\
            <div id = "tableScrollDiv-' + divID + '" style="overflow-x: scroll; overflow-y: hidden; height: '+(editorHeight+38)+'px; border: 1px solid #C4C4C4;border-radius:5px; float: left; display: block; margin-left: 0px; width: 50%" class="span6" >\
                <div class="divTable" id = "table-editor-' + divID + '" ></ div>\
            </div>\
        </div >\
        <div style = "border: 1px solid #C4C4C4; margin-right: 8px;border-radius:5px;display:'+ controlPanelDisplay+';width:99.5%;" >\
            <label style = "vertical-align: bottom; display: inline-block;margin-left:10px;" >\
                <input type = "checkbox" id = "livePreview-' + divID + '" checked = "checked" style ="vertical-align:top" / > Live Preview\
            </label>\
            <label style = "vertical-align: bottom; display: inline-block;margin-left:10px;" >\
                <input type = "checkbox" id = "liveDebugger-' + divID + '" checked = "checked" style ="vertical-align:top" / > Debugging\
            </label >\
            <button id="runButton-' + divID + '" class="btn btn-success" value = "Run" disabled = "disabled" style ="margin-left:10px;" / > Run </button >\
            <div id = "mousPos-' + divID + '" style = "display:inline-block;margin-left:10px;" > (0, 0) </div > \
        </div>\
        <div class="row" style = "text-align: center" >\
        <canvas id = "myCanvas-' + divID + '" height = "'+canvasHeight+'" width = "'+canvasWidth+'" style = "border: 1px solid #000000;border-radius:5px; cursor: crosshair;" >< / canvas >\
        </div>'

        $('#' + divID).html(html);
        
        var ed = new EditorModule.Editor("editor" + '-' + divID, "table-editor" + '-' + divID, "tableScrollDiv" + '-' + divID, "myCanvas" + '-' + divID, "livePreview" + '-' + divID, "runButton" + '-' + divID, "liveDebugger" + '-' + divID, "mousPos" + '-' + divID);
        ed.aceEditorOnChange();
    }
}

window.onload = () => {
    $('.editor-container').each( function (index,value) {
        editorGenerator.generateEditor($(value).attr('id'), true);
    });
    var divID = "debugger-example";
    $('#' + "tableScrollDiv" + '-' + divID).css('border', '2px solid #f00');
    divID = "";
}