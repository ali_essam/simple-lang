/// <reference path="Language.ts"/>

module Language {

    export enum NodeTypes {
        UNARY_OPERATOR, BINARY_OPERATOR, NUM_LITERAL, STRING_LITERAL, ID, STATEMENT, FUNCTION_CALL
    }

    export enum StatementTypes {
        BIND, IF, WHILE, FOR, STAT_LIST, DRAW
    }

    export enum OperatorTypes {
        ADD, SUB, MUL, DIV, MOD, OR, AND, EQUALS, NOT_EQUALS, GREATER, GREATER_EQUAL, SMALLER, SMALLER_EQUAL, NOT, NEGATIVE, POW
    }

    export enum DrawTypes {
        CIRCLE, RECT,LINE,TEXT
    }

    export class Node {
        constructor(public nodeType: NodeTypes, public token: Token) { }
    }

    export class Statement extends Node {
        constructor(public type: StatementTypes, token: Token) { super(NodeTypes.STATEMENT, token) }
    }

    export class Point {
        constructor(public x: Node, public y: Node) { }
    }

    export class DrawStat extends Statement {
        keyValues: Node[];
        constructor(public drawType: DrawTypes, token: Token) {
            super(StatementTypes.DRAW, token);
            this.keyValues = new Array();
        }
    }

    export class DrawCircleStat extends DrawStat {
        constructor(public center: Point, token: Token) { super(DrawTypes.CIRCLE, token) }
    }

    export class DrawTextStat extends DrawStat {
        constructor(public center: Point,public value:Node, token: Token) { super(DrawTypes.TEXT, token) }
    }

    export class DrawRectStat extends DrawStat {
        constructor(public from: Point, public to: Point, token: Token) { super(DrawTypes.RECT, token) }
    }

    export class DrawLineStat extends DrawStat {
        constructor(public from: Point, public to: Point, token: Token) { super(DrawTypes.LINE, token) }
    }

    export class BindStatement extends Statement {
        constructor(public identifier: string, public value: Node, token: Token) { super(StatementTypes.BIND, token) }
    }

    export class IfData {
        constructor(public condition: Node, public body: StatementList) { }
    }

    export class IfStatement extends Statement {
        constructor(public data: IfData[], token: Token) { super(StatementTypes.IF, token) }
    }

    export class WhileStatement extends Statement {
        constructor(public condition: Node, public body: StatementList, token: Token) { super(StatementTypes.WHILE, token) }
    }

    export class ForStatement extends Statement {
        constructor(public id: string, public iterable: Node, public body: StatementList, token: Token) { super(StatementTypes.FOR, token) }
    }

    export class StatementList extends Statement {
        constructor(public statements: Statement[]) { super(StatementTypes.STAT_LIST, null) }
    }

    export class UnaryOperator extends Node {
        constructor(public operator: OperatorTypes, public operand: Node, token: Token) { super(NodeTypes.UNARY_OPERATOR, token) }
    }

    export class BinaryOperator extends Node {
        constructor(public operator: OperatorTypes, public operand1: Node, public operand2: Node, token: Token) { super(NodeTypes.BINARY_OPERATOR, token) }
    }

    export class NumLiteral extends Node {
        constructor(public value: number, token: Token) { super(NodeTypes.NUM_LITERAL, token) }
    }

    export class Identifier extends Node {
        constructor(public name: string, token: Token) { super(NodeTypes.ID, token) }
    }

    export class StringLiteral extends Node {
        constructor(public value: string, token: Token) { super(NodeTypes.STRING_LITERAL, token) }
    }

    export class FunctionCall extends Node {
        constructor(public id: String, public parameterList: Node[], token: Token) { super(NodeTypes.FUNCTION_CALL, token) }
    }


    export class Parser {
        private Tokens: Token[];
        p: number;
        hasErrors: boolean = false;

        public updateTokenList(_Tokens: Token[]) {
            this.Tokens = _Tokens;
            this.hasErrors = false;
            this.p = 0;
        }

        //Tools
        private hasMoreTokens() {
            return (this.p < this.Tokens.length);
        }

        private retract() {
            this.p--;
        }

        private move() {
            this.p++;
        }

        private read(): Token {
            return this.Tokens[this.p++];
        }

        private EOF(): Token {
            return new Token("", TokenTypes.EOF, -1, -1, -1, "");
        }

        private check(inp: TokenTypes): boolean {
            if (this.hasMoreTokens()) {
                return this.Tokens[this.p].type == (inp);
            }
            return false;
        }

        private match(inp: any) {
            if (this.hasMoreTokens() && this.check(inp)) {
                this.move();
                return true;
            }
            else {
                console.warn("Match Error");//ERROR
                this.hasErrors = true;
            }
        }

        private lookAhead(): Token {
            if (this.hasMoreTokens()) {
                return this.Tokens[this.p];
            }
            else {
                console.error("LookAheadError");//ERROR
                this.hasErrors = true;
            }
        }

        //End Tools

        //Parsing Functions

        parse(): Node {
            this.p = 0;
            return this.stat_list(true);
        }

        private stat_list(canBeEmpty: boolean= false): StatementList {
            var stat: Statement;
            var stat_array: Statement[] = new Array();
            while (true) {
                stat = this.stat();
                if (stat != null) {
                    stat_array.push(stat);
                }
                else {
                    if (this.check(TokenTypes.EOL)) this.move();
                    else break;
                }
            }
            if (!canBeEmpty && stat_array.length == 0) {
                this.hasErrors = true;
                console.error("Empty Stat List Error")//ERROR
            }
            return new StatementList(stat_array);
        }

        private stat(): Statement {
            var stat = this.compound_stat();
            if (stat == null) stat = this.simple_stat();
            return stat;
        }

        private simple_stat(): Statement {
            var stat: Statement = this.let_stat();
            if (stat == null) stat = this.draw_stat();
            if (stat != null) this.match(TokenTypes.EOL);
            return stat;
        }

        private compound_stat(): Statement {
            var stat: Statement = this.if_stat();
            if (stat == null) stat = this.while_stat();
            if (stat == null) stat = this.for_stat();
            return stat;
        }

        private if_stat(): Statement {
            var ifData: IfData[] = new Array();
            if (this.check(TokenTypes.IF)) {
                var ifToken: Token = this.read();
                var condition: Node = this.expression();
                this.match(TokenTypes.EOL);
                this.match(TokenTypes.INDENT);
                var body: StatementList = this.stat_list();
                this.match(TokenTypes.DEDENT);
                ifData.push(new IfData(condition, body));

                while (this.check(TokenTypes.ELSE)) {
                    var elseToken: Token = this.read();
                    var condition: Node;
                    if (this.check(TokenTypes.IF)) {
                        this.move();
                        condition = this.expression();
                    }
                    else {
                        condition = new NumLiteral(1, elseToken);//else stat
                    }
                    this.match(TokenTypes.EOL);
                    this.match(TokenTypes.INDENT);
                    var body: StatementList = this.stat_list();
                    this.match(TokenTypes.DEDENT);
                    ifData.push(new IfData(condition, body));
                }
                return new IfStatement(ifData, ifToken);
            }
            return null;
        }

        private while_stat(): Statement {
            if (this.check(TokenTypes.WHILE)) {
                var whileToken: Token = this.read();
                var condition: Node = this.expression();
                this.match(TokenTypes.EOL);
                this.match(TokenTypes.INDENT);
                var body: StatementList = this.stat_list();
                this.match(TokenTypes.DEDENT);
                return new WhileStatement(condition, body, whileToken);
            }
        }

        private for_stat(): Statement {
            if (this.check(TokenTypes.FOR)) {
                var forToken: Token = this.read();
                var idToken: Token = this.read();
                this.match(TokenTypes.IN);
                var range: Node = this.expression();
                this.match(TokenTypes.EOL);
                this.match(TokenTypes.INDENT);
                var body: StatementList = this.stat_list();
                this.match(TokenTypes.DEDENT);
                return new ForStatement(idToken.lexeme, range, body, forToken);
            }
        }

        //#region draw

        private draw_stat(): DrawStat {
            var stat: DrawStat;
            if (this.check(TokenTypes.DRAW)) {
                this.move();
                stat = this.draw_circle();
                if (stat == null) stat = this.draw_rect();
                if (stat == null) stat = this.draw_line();
                if (stat == null) stat = this.draw_text();
            }
            if (stat != null) {
                if (this.check(TokenTypes.WITH)) {
                    this.move();
                    var key: string;
                    if (this.check(TokenTypes.ID)) {
                        key = this.read().lexeme;
                        this.match(TokenTypes.EQUAL);
                        stat.keyValues[key] = this.expression();
                        while (this.check(TokenTypes.COMMA)) {
                            this.move();
                            if (this.check(TokenTypes.ID)) {
                                key = this.read().lexeme;
                                this.match(TokenTypes.EQUAL);
                                stat.keyValues[key] = this.expression();
                            }
                            else {
                                console.error("ID Expected");//Error
                                this.hasErrors = true;
                            }
                        }
                    }
                    else {
                        console.error("ID Expected");//Error
                        this.hasErrors = true;
                    }
                }
            }
            return stat;
        }

        private draw_text(): DrawTextStat {
            if (this.check(TokenTypes.TEXT)) {
                var textToken: Token = this.read();
                var value: Node = this.expression();
                this.match(TokenTypes.AT);
                var center: Point = this.point();
                return new DrawTextStat(center,value, textToken);
            }
        }

        private draw_circle(): DrawCircleStat {
            if (this.check(TokenTypes.CIRCLE)) {
                var circleToken: Token = this.read();
                this.match(TokenTypes.AT);
                var center: Point = this.point();
                return new DrawCircleStat(center, circleToken);
            }
        }

        private draw_rect(): DrawRectStat {
            if (this.check(TokenTypes.RECTANGLE)) {
                var rectToken: Token = this.read();
                this.match(TokenTypes.FROM);
                var from: Point = this.point();
                this.match(TokenTypes.TO);
                var to: Point = this.point();
                return new DrawRectStat(from, to, rectToken);
            }
        }

        private draw_line(): DrawLineStat {
            if (this.check(TokenTypes.LINE)) {
                var lineToken: Token = this.read();
                this.match(TokenTypes.FROM);
                var from: Point = this.point();
                this.match(TokenTypes.TO);
                var to: Point = this.point();
                return new DrawLineStat(from, to, lineToken);
            }
        }

        private point() {
            this.match(TokenTypes.L_PARA);
            var x: Node = this.expression();
            this.match(TokenTypes.COMMA);
            var y: Node = this.expression();
            this.match(TokenTypes.R_PARA);
            return new Point(x, y);
        }
        //#endregion

        private let_stat(): BindStatement {
            if (this.check(TokenTypes.LET)) {
                var letToken: Token = this.read();
                var id: Token = this.read();
                this.match(TokenTypes.BE);
                var val: Node = this.expression();
                return new BindStatement(id.lexeme, val, letToken);
            }
            return null;
        }

        //#region expressions
        private expression(): Node {
            return this.logical_expr();
        }

        private logical_expr(): Node {
            var left: Node = this.comp_expr();
            while (this.check(TokenTypes.AND) || this.check(TokenTypes.OR)) {
                var op: Token = this.read();
                var opType: OperatorTypes;
                if (op.type == TokenTypes.AND) {
                    opType = OperatorTypes.AND;
                }
                else if (op.type == TokenTypes.OR) {
                    opType = OperatorTypes.OR;
                }
                var right: Node = this.comp_expr();
                left = new BinaryOperator(opType, left, right, op);
            }
            return left;
        }

        private comp_expr(): Node {
            var left: Node = this.add_expr();
            while (this.check(TokenTypes.GREATER) || this.check(TokenTypes.GREATER_EQUAL) || this.check(TokenTypes.SMALLER_EQUAL)
                || this.check(TokenTypes.SMALLER) || this.check(TokenTypes.SMALLER_EQUAL)
                || this.check(TokenTypes.EQUAL) || this.check(TokenTypes.NOT_EQUAL))
            {
                var op: Token = this.read();
                var opType: OperatorTypes;
                if (op.type == TokenTypes.GREATER_EQUAL) {
                    opType = OperatorTypes.GREATER_EQUAL;
                }
                else if (op.type == TokenTypes.GREATER) {
                    opType = OperatorTypes.GREATER;
                }
                else if (op.type == TokenTypes.SMALLER) {
                    opType = OperatorTypes.SMALLER;
                }
                else if (op.type == TokenTypes.SMALLER_EQUAL) {
                    opType = OperatorTypes.SMALLER_EQUAL;
                }
                else if (op.type == TokenTypes.EQUAL) {
                    opType = OperatorTypes.EQUALS;
                }
                else if (op.type == TokenTypes.NOT_EQUAL) {
                    opType = OperatorTypes.NOT_EQUALS;
                }
                var right: Node = this.add_expr();
                left = new BinaryOperator(opType, left, right, op);
            }
            return left;
        }

        private add_expr(): Node {
            var left: Node = this.mul_expr();
            while (this.check(TokenTypes.PLUS) || this.check(TokenTypes.MINUS)) {
                var op: Token = this.read();
                var opType: OperatorTypes;
                if (op.type == TokenTypes.PLUS) {
                    opType = OperatorTypes.ADD;
                }
                else if (op.type == TokenTypes.MINUS) {
                    opType = OperatorTypes.SUB;
                }
                var right: Node = this.mul_expr();
                left = new BinaryOperator(opType, left, right, op);
            }
            return left;
        }

        private mul_expr(): Node {
            var left: Node = this.pow_expr();
            while (this.check(TokenTypes.MUL) || this.check(TokenTypes.DIV) || this.check(TokenTypes.MOD)) {
                var op: Token = this.read();
                var opType: OperatorTypes;
                if (op.type == TokenTypes.MUL) {
                    opType = OperatorTypes.MUL;
                }
                else if (op.type == TokenTypes.DIV) {
                    opType = OperatorTypes.DIV;
                }
                else if (op.type == TokenTypes.MOD) {
                    opType = OperatorTypes.MOD;
                }
                var right: Node = this.pow_expr();
                left = new BinaryOperator(opType, left, right, op);
            }
            return left;
        }

        private pow_expr(): Node {
            var left: Node = this.factor();
            if (this.check(TokenTypes.POW)) {
                var op: Token = this.read();
                var right: Node = this.pow_expr();
                left = new BinaryOperator(OperatorTypes.POW, left, right, op);
            }
            return left;
        }

        private factor(): Node {
            if (this.check(TokenTypes.NUMBER)) {
                var tok: Token = this.read();
                var value = parseFloat(tok.lexeme);
                return new NumLiteral(value, tok);
            }
            else if (this.check(TokenTypes.ID)) {
                var func_call = this.function_call();
                if (func_call != null) return func_call;
                var tok: Token = this.read();
                var id = tok.lexeme;
                return new Identifier(id, tok);
            }
            else if (this.check(TokenTypes.STRING)) {
                var tok: Token = this.read();
                var str = tok.lexeme;
                return new StringLiteral(str, tok);
            }
            else if (this.check(TokenTypes.L_PARA)) {
                this.move();// (
                var node: Node = this.expression();
                this.match(TokenTypes.R_PARA);
                return node;
            }
            else if (this.check(TokenTypes.MINUS)) {
                var tok: Token = this.read();// -
                var node: Node = this.factor();
                return new UnaryOperator(OperatorTypes.NEGATIVE, node, tok);
            }
            else if (this.check(TokenTypes.NOT)) {
                var tok: Token = this.read();// not
                var node: Node = this.factor();
                return new UnaryOperator(OperatorTypes.NOT, node, tok);
            }
            console.error("factor error");
            this.hasErrors = true;
            return null;//ERROR
        }

        function_call(): Node {
            if (this.lookAhead().type == TokenTypes.ID) {
                var id = this.read();
                if (this.lookAhead().type == TokenTypes.L_PARA) {
                    this.move();
                    var parameterArray: Node[] = this.expression_list();//TODO
                    this.match(TokenTypes.R_PARA);
                    return new FunctionCall(id.lexeme, parameterArray, id);
                }
                else {
                    this.retract();
                    return null;
                }
            }
        }

        expression_list(canBeEmpty: boolean= true): Node[] {
            var parameterArray: Node[] = new Array();
            var tempExpr: Node
            if (!canBeEmpty) {
                tempExpr = this.expression();
                parameterArray.push(tempExpr);
            }
            else {
                if (!this.check(TokenTypes.R_PARA)) {
                    tempExpr = this.expression();
                    parameterArray.push(tempExpr);
                }
            }
            while (this.check(TokenTypes.COMMA)) {
                this.read();
                tempExpr = this.expression();
                parameterArray.push(tempExpr);
            }
            return parameterArray;
        }
        //#endregion
    }
}