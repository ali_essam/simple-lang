/// <reference path="Language.ts"/>

module Language {
    export enum ObjectTypes {
        NUMBER, STRING, STAT, ARRAY
    }


    export class LangObject {

        public value;

        constructor(public type: ObjectTypes) { }

        add(other: LangObject): LangObject { return null; }//abstract
        sub(other: LangObject): LangObject { return null; }//abstract
        mul(other: LangObject): LangObject { return null; }//abstract
        div(other: LangObject): LangObject { return null; }//abstract
        mod(other: LangObject): LangObject { return null; }//abstract
        pow(other: LangObject): LangObject { return null; }//abstract

        smaller(other: LangObject): boolean { return null; }//abstract
        greaterEqual(other: LangObject): boolean {
            return !this.smaller(other);
        }
        greater(other: LangObject): boolean {
            return this.greaterEqual(other) && !this.equal(other);
        }
        smallerEqual(other: LangObject): boolean {
            return this.smaller(other) || this.equal(other);
        }
        equal(other: LangObject): boolean {
            return !(this.smaller(other) || other.smaller(this));
        }
        notEqual(other: LangObject): boolean {
            return !this.equal(other);
        }

        and(other: LangObject): boolean { return null; }//abstract
        or(other: LangObject): boolean { return null; }//abstract

        not(): LangObject { return null; }//abstract
        negative(): LangObject { return null; }//abstract

        //#region iterators

        isIterable: boolean = false;

        initIterator() { }
        hasMoreItems(): boolean { return false; }
        nextItem(): LangObject { return null; }

        //#endregion
    }

    export class NumberObject extends LangObject {
        constructor(public value: number) { super(ObjectTypes.NUMBER) }

        add(other: LangObject): LangObject {
            if (other.type == ObjectTypes.NUMBER) {
                var _other: NumberObject = <NumberObject>other;
                return new NumberObject(this.value + _other.value);
            }
            else if (other.type == ObjectTypes.STRING) {
                return (<StringObject>other).add(this,true);
            }

        }//abstract
        sub(other: LangObject): LangObject {
            if (other.type == ObjectTypes.NUMBER) {
                var _other: NumberObject = <NumberObject>other;
                return new NumberObject(this.value - _other.value);
            }
        }//abstract
        mul(other: LangObject): LangObject {
            if (other.type == ObjectTypes.NUMBER) {
                var _other: NumberObject = <NumberObject>other;
                return new NumberObject(this.value * _other.value);
            }
        }//abstract
        div(other: LangObject): LangObject {
            if (other.type == ObjectTypes.NUMBER) {
                var _other: NumberObject = <NumberObject>other;
                return new NumberObject(this.value / _other.value);
            }
        }//abstract
        mod(other: LangObject): LangObject {
            if (other.type == ObjectTypes.NUMBER) {
                var _other: NumberObject = <NumberObject>other;
                return new NumberObject(this.value % _other.value);
            }
        }//abstract
        pow(other: LangObject): LangObject {
            if (other.type == ObjectTypes.NUMBER) {
                var _other: NumberObject = <NumberObject>other;
                return new NumberObject(Math.pow(this.value, _other.value));
            }
        }//abstract

        smaller(other: LangObject): boolean {
            if (other.type == ObjectTypes.NUMBER) {
                var _other: NumberObject = <NumberObject>other;
                return this.value < _other.value;
            }
        }


        and(other: LangObject): boolean {
            if (other.type == ObjectTypes.NUMBER) {
                var _other: NumberObject = <NumberObject>other;
                return NumberObject.toBool(this) && NumberObject.toBool(_other);
            }
        }//abstract
        or(other: LangObject): boolean {
            if (other.type == ObjectTypes.NUMBER) {
                var _other: NumberObject = <NumberObject>other;
                return NumberObject.toBool(this) || NumberObject.toBool(_other);
            }
        }//abstract


        not(): LangObject {
            if (this.value == 0) {
                this.value = 1;
            }
            else {
                this.value = 0;
            }
            return this;
        }//abstract
        negative(): LangObject {
            this.value = -this.value;
            return this;
        }//abstract

        static fromBool(b: boolean): NumberObject {
            if (b) {
                return new NumberObject(1);
            }
            else {
                return new NumberObject(0);
            }
        }

        static toBool(obj: NumberObject): boolean {
            if (obj.value != 0) {
                return true;
            }
            else {
                return false;
            }
        }

    }
    export class StringObject extends LangObject {
        it: number;
        constructor(public value: string) {
            super(ObjectTypes.STRING);
            this.isIterable = true;
        }

        add(other: LangObject, reversed: boolean = false): LangObject {
            if (!reversed)
                return new StringObject(this.value + other.value);
            else
                return new StringObject(other.value + this.value);
        }

        initIterator() { this.it = 0; }
        hasMoreItems(): boolean { return this.it < this.value.length; }
        nextItem(): LangObject { return new StringObject(this.value[this.it++]); }
    }
    export class ArrayObject extends LangObject {
        it: number;
        constructor(public value: LangObject[]) {
            super(ObjectTypes.ARRAY);
            this.isIterable = true;
        }
        initIterator() { this.it = 0; }
        hasMoreItems(): boolean { return this.it < this.value.length; }
        nextItem(): LangObject { return this.value[this.it++]; }
    }

    export class StatObject extends LangObject {
        constructor(public value: string) { super(ObjectTypes.STAT) }
    }

}