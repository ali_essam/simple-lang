/// <reference path="Language.ts"/>
/// <reference path="ace-src-noconflict/ace.d.ts"/>
/// <reference path="jquery.d.ts"/>
/// <reference path="Editor.ts"/>


var canv: HTMLCanvasElement;
var langManager: Language.LangManager;
var ed: EditorModule.Editor;

window.onload = () => {
    canv = <HTMLCanvasElement>document.getElementById("myCanvas");
    ed = new EditorModule.Editor("editor", "table-editor", "tableScrollDiv", "myCanvas", "livePreview", "runButton", "liveDebugger","mousPos");
    //langManager = new Language.LangManager(canv);
};